/*
	JSON Syntax

			{

					"key1": "valueA",
					"key2": "valueB",

			}
		Keys wrappd in curly braces
*/


let sample1 = 
`
		{
			"name": "Katniss Everdeen",
			"age": "21",
			"address": 
			{
				"city": "Kansas City",
				"state": " Kansas"

			}

		}

`
console.log(sample1);

// Are we able to turn a JSON in to JS Object

console.log(JSON.parse(sample1));

let sampleArr = `
	[

		{

			"email": "jasonNewsted@gmail.com",
			"password": "isplaybass1234",
			"isAdmin": false
		},
		{

			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}


			]

`;

// console.log(sampleArr);

// // let parsedSampleArr = JSON.parse(sampleArr);
// console.log(parsedSampleArr);
// console.log(parsedSampleArr.pop())
// console.log(parsedSampleArr);

// console.log(sampleArr);

// sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

// Database(JSON) => Server/API (JSON to JS object to process) => sent as JSON => client


let jsonArr = `
	[
			"pizza",
			"hamburger",
			"spaghetti",
			"shanghai",
			"hotdog stick on a pineapple",
			"pancit bihon"

	]
`;

console.log(jsonArr);
let parsedJsonArr = JSON.parse(jsonArr);
console.log(parsedJsonArr);
console.log(parsedJsonArr.pop());
console.log(parsedJsonArr.push("adobo"));
jsonArr = JSON.stringify(parsedJsonArr);
console.log(jsonArr);